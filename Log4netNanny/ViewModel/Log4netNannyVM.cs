﻿using Log4netNanny.Model;
using Log4netNanny.Utility;
using Log4netNanny.ViewModel;
using Microsoft.Web.Administration;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;


namespace ViewModel.Log4netNanny
{
    using SystemMessageBox = System.Windows.MessageBox;
    public class Log4netNannyVM : ILog4netNanny
    {
        #region Variables
        private Log4netHelper _log4netHelper;
        private DispatcherTimer _dispatcherTimer;
        private string _logDirectory = string.Empty;
        private double _deleteInterval = 15;
        private string _appPoolName = string.Empty;
        private bool _isConfirmedForRecyle = false;
        private ICommand _openFileDialog;
        private ICommand _startTimerCommnad;
        private ICommand _stopTimerCommand;
        private ICommand _verifyAppPoolNameCommand;
        private ICommand _recycleAppPoolCommand;
        #endregion

        #region Properties
        public string LogDirectory
        {
            get => _logDirectory;
            set
            {
                if (_logDirectory != value)
                {
                    _logDirectory = value;
                    OnPropertyChanged(nameof(LogDirectory));
                }
            }
        }

        public double DeleteInterval
        {
            get => _deleteInterval;
            set
            {
                if (_deleteInterval != value)
                {
                    _deleteInterval = value;
                    _dispatcherTimer.Interval = TimeSpan.FromDays(_deleteInterval);
                    //_dispatcherTimer.Interval = TimeSpan.FromSeconds(_deleteInterval);
                    OnPropertyChanged(nameof(DeleteInterval));
                }
            }
        }

        public string AppPoolName
        {
            get => _appPoolName;
            set
            {
                if (_appPoolName != value)
                {
                    _appPoolName = value;
                    OnPropertyChanged(nameof(AppPoolName));
                }
            }
        }

        public bool IsConfirmedForRecyle
        {
            get => _isConfirmedForRecyle;
            set
            {
                if (_isConfirmedForRecyle != value)
                {
                    _isConfirmedForRecyle = value;
                    OnPropertyChanged(nameof(IsConfirmedForRecyle));
                }
            }
        }

        public ICommand OpenFileDialog
        {
            get
            {
                if (_openFileDialog == null)
                    _openFileDialog = new RelayCommand(c => true, c => OnOpenFileDialog());

                return _openFileDialog;
            }
        }

        public ICommand StartTimerCommand
        {
            get
            {
                if (_startTimerCommnad == null)
                    _startTimerCommnad = new RelayCommand(c => CanExecuteCleanup() && !_dispatcherTimer.IsEnabled, c => OnStartTimer());

                return _startTimerCommnad;
            }
        }

        public ICommand StopTimerCommand
        {
            get
            {
                if (_stopTimerCommand == null)
                    _stopTimerCommand = new RelayCommand(c => _dispatcherTimer.IsEnabled, c => OnStopTimer());

                return _stopTimerCommand;
            }
        }

        public ICommand VerifyAppPoolNameCommand
        {
            get
            {
                if (_verifyAppPoolNameCommand == null)
                    _verifyAppPoolNameCommand = new RelayCommand(c => !string.IsNullOrEmpty(AppPoolName), c => OnVerifyAppPoolName());

                return _verifyAppPoolNameCommand;
            }
        }

        public ICommand RecycleAppPoolCommand
        {
            get
            {
                if (_recycleAppPoolCommand == null)
                    _recycleAppPoolCommand = new RelayCommand(c => IsConfirmedForRecyle, c => OnRecycleAppPool());

                return _recycleAppPoolCommand;
            }
        }
        #endregion

        #region CTOR
        public Log4netNannyVM()
        {
            _log4netHelper = new Log4netHelper();

            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Interval = TimeSpan.FromDays(DeleteInterval);
            //_dispatcherTimer.Interval = TimeSpan.FromSeconds(DeleteInterval); TEST
            _dispatcherTimer.Tick += Log4netMonitor_Tick;
        }
        #endregion

        #region Event Handlers
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        private void Log4netMonitor_Tick(object sender, EventArgs e)
        {
            _log4netHelper.Cleanup(LogDirectory);
        }

        private bool CanExecuteCleanup()
        {
            return !string.IsNullOrEmpty(LogDirectory);
        }

        private void OnStartTimer() => _dispatcherTimer.Start();

        private void OnStopTimer() => _dispatcherTimer.Stop();

        private void OnOpenFileDialog()
        {
            var folderBrowser = new FolderBrowserDialog();

            var result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
                LogDirectory = folderBrowser.SelectedPath;
        }

        private void OnVerifyAppPoolName()
        {
            var isExisting = false;
            IsConfirmedForRecyle = false;

            var message = "Please check your api app pool name.";

            using (ServerManager iisManager = new ServerManager())
            {
                SiteCollection sites = iisManager.Sites;

                var match = sites.FirstOrDefault(s => s.Name == AppPoolName);

                if (match != null)
                    isExisting = true;
            }

            if (isExisting)
            {
                message = "Ready to recylyc your api app pool.";

                SystemMessageBox.Show(message, "Ready", MessageBoxButton.OK, MessageBoxImage.Information);
                IsConfirmedForRecyle = true;
            }
            else
            {
                SystemMessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OnRecycleAppPool()
        {
            using (ServerManager iisManager = new ServerManager())
            {
                SiteCollection sites = iisManager.Sites;

                foreach (Site site in sites)
                {
                    if (site.Name == AppPoolName)
                    {
                        iisManager.ApplicationPools[site.Applications["/"].ApplicationPoolName].Recycle();
                        break;
                    }
                }
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        #endregion
    }
}
