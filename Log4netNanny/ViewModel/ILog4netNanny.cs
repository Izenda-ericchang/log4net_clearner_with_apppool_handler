﻿using System.ComponentModel;
using System.Windows.Input;

namespace Log4netNanny.ViewModel
{
    public interface ILog4netNanny : INotifyPropertyChanged
    {
        #region Properties
        string LogDirectory { get; set; }

        double DeleteInterval { get; set; }

        string AppPoolName { get; set; }

        ICommand OpenFileDialog { get; }

        ICommand StartTimerCommand { get; }

        ICommand StopTimerCommand { get; }

        ICommand VerifyAppPoolNameCommand { get; }

        ICommand RecycleAppPoolCommand { get; }
        #endregion
    }
}
