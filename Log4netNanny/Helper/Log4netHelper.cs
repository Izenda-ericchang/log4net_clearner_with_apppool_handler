﻿using System;
using System.IO;

namespace Log4netNanny.Model
{
    public class Log4netHelper
    {
        #region Methods
        internal void Cleanup(string directory)
        {
            var dirInfo = new DirectoryInfo(directory);
            if (!dirInfo.Exists)
            {
                throw new Exception("Unkown directory information");
            }

            var files = dirInfo.GetFiles();

            Array.Sort(files, delegate (FileInfo f1, FileInfo f2)
            {
                return f1.CreationTime.CompareTo(f2.CreationTime);
            });

            var filesCount = files.Length;

            if (filesCount > 20)
            {
                for (int i = 20; i <= filesCount - 1; i++)
                {
                    if (files[i].Name != "izenda-log.log")
                        files[i].Delete();
                }
            }
          
            //foreach (var file in files)
            //{
            //    //if (file.Name != "izenda-log.log" || file.LastAccessTime < DateTime.Now.AddDays(-1))
            //    if (file.Name != "izenda-log.log")
            //        file.Delete();
            //}
        } 
        #endregion
    }
}
