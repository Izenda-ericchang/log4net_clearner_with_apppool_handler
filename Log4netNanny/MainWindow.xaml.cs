﻿using System.Windows;
using ViewModel.Log4netNanny;

namespace Log4netNanny
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new Log4netNannyVM();
        }
    }
}
